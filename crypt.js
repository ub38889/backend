const bcrypt = require('bcrypt'); // descargamos bcrypt
function hash(data){// return un string encriptado. El que devuelve hashSync

  console.log("hashing data");

  return bcrypt.hashSync(data,10);
}



function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
 console.log("Checking password");

 return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

function checkEmail(email) {
 console.log("Checking email");
 var punto = 0;
 var contarrobas = 0;
 for (var i = 0; i<email.length;i++) {

				switch (email.charAt(i)) {
				case '.':
						punto = 1;
				break;
				case '@':
					 contarrobas ++;
				break;

				} // switch
			} // for

			if (email.charAt(0)== '.' || email.charAt(0)== '@') {
				punto = 0;
			}
			if (email.charAt(email.length-1)== '.' || email.charAt(email.length-1)== '@') {
				punto = 0;
			}
			if (punto == 1 && contarrobas == 1) {
				return  "OK";
				System.out.println("El Email es correcto");
			} else {
				return  "KO";
				System.out.println("El Email no es correcto");
			}


}


module.exports.hash = hash;


module.exports.checkPassword = checkPassword;

module.exports.checkEmail = checkEmail;
