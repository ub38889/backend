// Con el REQUIRE importamos el fichero desde la ruta indicada
const io = require('../io');

const crypt = require('../crypt');

const requestJson = require('request-json');



var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/movimientos";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;// la clave ya no está en el código sino que es una referencia del fichero .env

console.log(mLabAPIKey);


function getMovimientosByIBANV2(req, res) { // devuelve las cuentas de un usuario con una id concreta
       console.log("GET /apitechu/v2/movimientos/:IBAN");

       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                              //con la url  para acceder a la coleccion user


       //var identusu = req.params.IdUsuario; //  la variable ident es el identificador que llega en el parámetro.
       var identiban = req.params.IBAN; //  la variable ident es el identificador que llega en el parámetro.

      // var query = 'q={"IdUsuario":' + identusu + "&" + '"IBAN":"' + identiban + '""}';
       var query = 'q={"IBAN":"' + identiban + '"}';





       console.log("query1" + query);
       console.log("query2" + "movimientos?" + query + "&" + mLabAPIKey);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("movimientos?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          if (err){
            var response  = {"msg" : "Error obteniendo los movimientos por id de usuarios"}
            res.status(500);
          }  else { if (body.length > 0 ){ // Si el body tiene contenido
                        var response = body;

                        console.log("respuesta " + body);
                        res.send(response); // Se envía el response

                    } else {
                        var response ={"msg" : "Usuario no encontrado"}
                        res.status(404);
                          }

                  }


        });
        //  var response = !err ?    // condicion ternaria: Si no es error se responde el contenido del body y sino el mensaje de error
        //  body[0] : { "msg" : "Error obteniendo los usuarios"}
}



function createMovimientoV2(req, res) {
    console.log("POST /apitechu/v2/movimientos");
    console.log("parámetros" );
    console.log( req.params);
    console.log("QueryString" );
    console.log( req.query);
    console.log("HEaders" );
    console.log(req.headers);
    console.log("Body" );
    console.log(req.body);

    var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";
    var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
    var identiban = req.body.IBAN; //  la variable ident es el identificador que llega en el parámetro

   var query = 'q={"IBAN":"' + identiban + '"}'; // la variable query es la condicion de que la id sea la que llega por parámetro

    console.log("query1" + query);
    console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
  //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
         httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
           if (err){
             var response  = {"msg" : "Error obteniendo las cuentas por IBAN"}
             res.status(500);
           }  else {
              if (body.length > 0 ){ // Si el body tiene contenido
                 let saldocta = body[0].Importe
                 let saldo = (body[0].Importe + req.body.Importe)
                 console.log("saldo " + saldo + " saldocta " + saldocta)
                 req.body.IdUsuario = body[0].iduser
                 if (saldo > 0){
                    console.log(" saldo positivo, vamos a dar de alta el movimiento" + req.body.IdUsuario)
                    var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/movimientos";
                    var httpClient = requestJson.createClient(baseMlabURL);
                    var d = new Date();
                    var time =d.getTime();
                    var dia = d.getDate();
                    var mes = (d.getMonth() + 1);
                    var ano = (d.getFullYear());
                    var fecha = dia + "-" + mes +"-" + ano;

                    var newMovto = { // Recuperamos estos parámetros del Body
                       "IdUsuario": req.body.IdUsuario,
                       "IBAN": req.body.IBAN,
                       "Concepto": req.body.Concepto,
                       "Importe": req.body.Importe,
                        "Fecha": fecha
                     }
       // damos de alta el movimiento en la base de datos movimientos
                    var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                    console.log("Lo primero es dar de alta el movimiento");
                    console.log(newMovto);
        // Para el post se necesitan tres parámetros: la http  , los datos del cliente y la funcion
                     httpClient.post("movimientos?" + mLabAPIKey, newMovto, function(err, resMLab, body){
                          console.log("movimiento creado en Mlab, ahora vamos a modificar el saldo en la cuenta");

                         //res.status(201).send({"msg":"movimiento guardado"}); // entendemos que ha finalizado ok con code status 201

                        // ahora vamos a modificar el saldo en la base de datos de cuentas
                         console.log("vamos a modificar el saldo de la cuenta")

                                       var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";
                                       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                       console.log(" vamos a modificar el saldo de la cuenta" + req.body.IBAN)
                                       var identiban = req.body.IBAN; //  la variable ident es el identificador que llega en el parámetro

                                       var query = 'q={"IBAN":"' + identiban + '"}'; // la variable query es la condicion de que la id sea la que llega por parámetro

                                       console.log("query1" + query);
                                       console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
                                       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
                                       httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){                                                        //con la url  para acceder a la coleccion user




                                      if (err){
                                        var response  = {"msg" : "Error obteniendo las cuentas por IBAN"}
                                        res.status(500);
                                        res.send(response);
                                      }  else {
                                            if (body.length > 0 ){ // Si el body tiene contenido

                                              Importeact = body[0].Importe + req.body.Importe;
                                              console.log("importeact " + Importeact);
                                              var putBody = '{"$set":{"Importe":' + Importeact + '}}';
                                              console.log(" putbody " +  putBody);
                                              httpClient.put("cuentas?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(errput, resMLabput, putBody){
                                              console.log("saldo modificado en cuentas en Mlab" + Importeact);


                                  //Vamos a recuperar el token con el id

                                                var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/user";
                                                var httpClient = requestJson.createClient(baseMlabURL);
                                                var ident = body[0].iduser
                                                var query = 'q={"iduser":' + ident + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro
                                                console.log(query);
                                    //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
                                                httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
                                                  if (err){
                                                    var response  = {"msg" : "Error obteniendo el token"}
                                                    res.status(500);
                                                    res.send(response);
                                                  }  else {
                                                    console.log( " token " + body[0].token)
                                                    var response ={ "mensaje" : "Operacion Realizada", "token" : body[0].token}
                                                    res.status(201);
                                                    res.send(response); // Se envía el response
                                                  }
                                                 })// usuarios para obtener el token
                                               })// ctas para modificar el saldo
                                             }
                                          }
                                       });// seleccioamos ctas por iban
                                   }) // damoa de alta el movimiento
                             }else{
                                 console.log("No damos de alta el movimiento porque se nos queda la cuenta en negativo")
                                 var response  = {"msg" : "No hay suficiente saldo para realizar la operacion",
                                                  "saldo" : saldocta}
                                 console.log( "saldocta " + saldocta)
                                 res.status(301);
                                 res.send(response); // Se envía el response
                             }// END-IF SALDO NEGATIVO;

                         }
                     }

               });


}

function createTraspasoV2(req, res) {
    console.log("POST createTraspasoV2");
    console.log("POST /apitechu/v2/movimientos");
    console.log("parámetros" );
    console.log( req.params);
    console.log("QueryString" );
    console.log( req.query);
    console.log("HEaders" );
    console.log(req.headers);
    console.log("Body" );
    console.log(req.body);

    var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";
    var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
    var identiban = req.body.IBANORIGEN; //  la variable ident es el identificador que llega en el parámetro

   var query = 'q={"IBAN":"' + identiban + '"}'; // la variable query es la condicion de que la id sea la que llega por parámetro

    console.log("query1" + query);
    console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
  //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
         httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){                                              // SELECT CTAS COMROBAR SALDO
           if (err){
             var response  = {"msg" : "Error obteniendo las cuentas por IBAN"}
             res.status(500);
           }  else {
              if (body.length > 0 ){ // Si el body tiene contenido
                 let saldocta = body[0].Importe
                 let saldo = (body[0].Importe - req.body.Importe)
                 console.log("saldo " + saldo + " saldocta " + saldocta)
                 req.body.IdUsuario = body[0].iduser
                 if (saldo > 0){
                    console.log(" saldo positivo, vamos a dar de alta el movimiento" + req.body.IdUsuario)
                    var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/movimientos";
                    var httpClient = requestJson.createClient(baseMlabURL);
                    var d = new Date();
                    var time =d.getTime();
                    var dia = d.getDate();
                    var mes = (d.getMonth() + 1);
                    var ano = (d.getFullYear());
                    var fecha = dia + "-" + mes +"-" + ano;

                    req.body.Concepto =  "Traspaso Origen"
                    let importeorigen = (req.body.Importe * (-1))


                    var newMovto = { // Recuperamos estos parámetros del Body
                       "IdUsuario": req.body.IdUsuario,
                       "IBAN": req.body.IBANORIGEN,
                       "Concepto": req.body.Concepto,
                       "Importe": importeorigen,
                        "Fecha": fecha
                     }


       // damos de alta el movimiento en la base de datos movimientos
                    var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                    console.log("Lo primero es dar de alta el movimiento");
                    console.log(newMovto);
        // Para el post se necesitan tres parámetros: la http  , los datos del cliente y la funcion
                     httpClient.post("movimientos?" + mLabAPIKey, newMovto, function(err, resMLab, body){                                            // INSERT MOVTO ORIGEN
                     console.log("movimiento creado en Mlab, ahora vamos a modificar el saldo en la cuenta");

                         //res.status(201).send({"msg":"movimiento guardado"}); // entendemos que ha finalizado ok con code status 201

                        // ahora vamos a modificar el saldo en la base de datos de cuentas
                    console.log("vamos a modificar el saldo de la cuenta")

                    var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";
                    var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                    console.log(" vamos a modificar el saldo de la cuenta" + req.body.IBANORIGEN)
                    var identiban = req.body.IBANORIGEN; //  la variable ident es el identificador que llega en el parámetro
                    var query = 'q={"IBAN":"' + identiban + '"}'; // la variable query es la condicion de que la id sea la que llega por parámetro
                   console.log("query1" + query);
                   console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
   //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
                   httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){                                            // Select cuenta origen
                   if (err){
                      var response  = {"msg" : "Error obteniendo las cuentas por IBAN"}
                                       res.status(500);
                                       res.send(response);
                    }  else {
                              if (body.length > 0 ){ // Si el body tiene contenido
                                  Importeact = body[0].Importe - req.body.Importe;
                                  console.log("importeact " + Importeact);
                                  var putBody = '{"$set":{"Importe":' + Importeact + '}}';
                                  console.log(" putbody " +  putBody);
                                  httpClient.put("cuentas?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(errput, resMLabput, putBody){        //update saldo de la cuenta origen
                                      console.log("saldo modificado en cuentas en Mlab" + Importeact);

    //Ahora hay que dar de alta el movimiento destino
                                      var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/movimientos";
                                      var httpClient = requestJson.createClient(baseMlabURL);
                                      var d = new Date();
                                      var time =d.getTime();
                                      var dia = d.getDate();
                                      var mes = (d.getMonth() + 1);
                                      var ano = (d.getFullYear());
                                      var fecha = dia + "-" + mes +"-" + ano;

                                      req.body.Concepto =  "Traspaso Destino"
                                      let importedestino = (req.body.Importe)


                                      var newMovto = { // Recuperamos estos parámetros del Body
                                         "IdUsuario": req.body.IdUsuario,
                                         "IBAN": req.body.IBANDESTINO,
                                         "Concepto": req.body.Concepto,
                                         "Importe": req.body.Importe,
                                          "Fecha": fecha
                                        }
        // damos de alta el movimiento en la base de datos movimientos
                                      var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                      console.log("Lo primero es dar de alta el movimiento");
                                      console.log(newMovto);
                                    // Para el post se necesitan tres parámetros: la http  , los datos del cliente y la funcion
                                       httpClient.post("movimientos?" + mLabAPIKey, newMovto, function(err, resMLab, body){                       //  insertar traspaso destino
                                           console.log("movimiento creado en Mlab, ahora vamos a modificar el saldo en la cuenta");

                                               //res.status(201).send({"msg":"movimiento guardado"}); // entendemos que ha finalizado ok con code status 201
       // ahora vamos a modificar el saldo en la base de datos de cuentas
                                           console.log("vamos a modificar el saldo de la cuenta")
                                           var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";
                                           var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                           console.log(" vamos a modificar el saldo de la cuenta" + req.body.IBANDESTINO)
                                           var identiban = req.body.IBANDESTINO; //  la variable ident es el identificador que llega en el parámetro

                                           var query = 'q={"IBAN":"' + identiban + '"}'; // la variable query es la condicion de que la id sea la que llega por parámetro

                                           console.log("query1" + query);
                                           console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
                                           //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
                                           httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){                            // acceder cuenta destino
                                                 Importeact = parseInt(body[0].Importe) + parseInt(req.body.Importe);
                                                 console.log("importeact " + Importeact);
                                                 var putBody = '{"$set":{"Importe":' + Importeact + '}}';
                                                 console.log(" putbody " +  putBody);
                                                 httpClient.put("cuentas?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(errput, resMLabput, putBody){   //update saldo destino
                                                 console.log("saldo modificado en cuentas en Mlab" + Importeact);
                                  //Vamos a recuperar el token con el id
                                                var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/user";
                                                var httpClient = requestJson.createClient(baseMlabURL);
                                                var ident = body[0].iduser
                                                var query = 'q={"iduser":' + ident + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro
                                                console.log(query);
                                    //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
                                                httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){                                            // obtener el token
                                                      if (err){
                                                        var response  = {"msg" : "Error obteniendo el token"}
                                                        res.status(500);
                                                        res.send(response);
                                                      }  else {
                                                        console.log( " token " + body[0].token)
                                                        var response ={ "mensaje" : "Operacion Realizada", "token" : body[0].token}
                                                        res.status(201);
                                                        res.send(response); // Se envía el response
                                                      } // END-IF
                                                 })   //              obtener el token
                                               })     //              update el saldo destino
                                             })       //              acceder cuenta destino
                                           })         //              Insertar Traspaso destino
                                        })             //             update saldo de la cuenta origen
                                      }                  //                end-if
                                   }                      //              END-IF
                                });                        //               select ctas origen por iban
                             })                           //             Insertar movto origen
                           }                              //              END-IF

                  }else{                                   //              END-IF

                                 console.log("No damos de alta el movimiento porque se nos queda la cuenta en negativo")
                                 var response  = {"msg" : "No hay suficiente saldo para realizar la operacion",
                                                  "saldo" : saldocta}
                                 console.log( "saldocta " + saldocta)
                                 res.status(301);
                                 res.send(response); // Se envía el response
                } // END-IF ;
          }// END-IF select ctas por iban

     });  //  // SELECT CTAS COMROBAR SALDO
} //  fin de la function traspaso



// Se incluyen los module.exports de las funciones para que se puedan invocar desde fuera


module.exports.getMovimientosByIBANV2 = getMovimientosByIBANV2;
module.exports.createMovimientoV2 = createMovimientoV2;
module.exports.createTraspasoV2 = createTraspasoV2;
