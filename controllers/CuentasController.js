// Con el REQUIRE importamos el fichero desde la ruta indicada
const io = require('../io');

const crypt = require('../crypt');

const requestJson = require('request-json');



var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;// la clave ya no está en el código sino que es una referencia del fichero .env

console.log(mLabAPIKey);


function getCuentasByIdV2(req, res) { // devuelve las cuentas de un usuario con una id concreta
       console.log("GET /apitechu/v2/cuentas/:id");

       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                              //con la url  para acceder a la coleccion user


       var ident = req.params.id; //  la variable ident es el identificador que llega en el parámetro.
       //
       // EN params.userid y en q={} hay que poner el nombre externo
       // en el caso de query ponemos el mismo nombre que se haya definido en MLAB
       //
       var query = 'q={"iduser":' + ident + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

       console.log("query1" + query);
       console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          if (err){
            var response  = {"msg" : "Error obteniendo las cuentas por id de usuarios"}
            res.status(500);
          }  else { if (body.length > 0 ){ // Si el body tiene contenido
                        var response = body;
                        console.log(body);

                    } else {
                        var response ={"msg" : "Usuario no encontrado"}
                        res.status(404);
                          }

                  }

          res.send(response); // Se envía el response
        });
        //  var response = !err ?    // condicion ternaria: Si no es error se responde el contenido del body y sino el mensaje de error
        //  body[0] : { "msg" : "Error obteniendo los usuarios"}
}



function getCuentasByIBANV2(req, res) { // Recupera una cuenta por un código IBAN

       console.log("get /apitechu/v2/cuentas/IBAN");

       console.log("vamos a empezar buscando el id que se correspode con el IBAN")



       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                              //con la url  para acceder a la coleccion user


       var identiban = req.params.IBAN; //  la variable ident es el identificador que llega en el parámetro.

       var query = 'q={"IBAN":' + identiban + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

       console.log("query1" + query);
       console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          if (err){
            var response  = {"msg" : "Error obteniendo las cuentas por IBAN"}
            res.status(500);
          }  else { if (body.length > 0 ){ // Si el body tiene contenido


                      var response = body;
                      console.log(body);


                    } else {
                        var response ={"msg" : "Cuenta no encontrado"}
                        res.status(404);
                          }

                  }

          res.send(response); // Se envía el response
        });
        //  var response = !err ?    // condicion ternaria: Si no es error se responde el contenido del body y sino el mensaje de error
        //  body[0] : { "msg" : "Error obteniendo los usuarios"}
}

function getCuentasByTokenV2(req, res) { // Recupera una cuenta por un código token

       console.log("get /apitechu/v3/cuentas/token");

      var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/user";

       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                              //con la url  para acceder a la coleccion user


       var token = req.params.token; //  la variable ident es el identificador que llega en el parámetro.

       var query = 'q={"token": ' + token + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

       console.log(query);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          if (err){
            var response  = {"msg" : "Error obteniendo los usuarios"}
            res.status(500);
          }  else { if (body.length > 0 ){ // Si el body tiene contenido
                        console.log("encontrado el id de user" + body[0].iduser);
                        console.log("vamos a buscar las cuentas por id");
                        console.log("cambiamos a la base de datas de cuentas ");

                        var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";
                        var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient




                        var iduser = body[0].iduser; //  la variable ident es el identificador que llega en el parámetro.

                        var query = 'q={"iduser":' + iduser + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

                        console.log("query1" + query);
                        console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);



                        //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
                        httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
                        //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros


                           if (err){
                             console.log("error acceso base de datos de cuenta")
                             var response  = {"msg" : "Error obteniendo las cuentas por IBAN"}
                             res.status(500);
                           }  else {
                                     console.log("ha salido del acceso da cuentas po ok " + body.length )
                                     if (body.length > 0 ){ // Si el body tiene contenido

                                       console.log("encontradas las cuentas del token por el iduser ")
                                       var response = body;

                                       console.log(body);
                                       res.send(response); // Se envía el response
                                       res.status(201);

                                     } else {
                                         console.log("no hay cuentas");
                                         var response ={"msg" : "Cliente sin cuentas"}
                                         res.status(404);
                                           }

                                   }

                         });

                    } else {
                        var response ={"msg" : "Usuario no encontrado"}
                        res.status(404);
                          }
                  }

        });
}

function postCuentasByIBANV2(req, res) { // modifica el saldo de una cuenta por IBAN

       console.log("POST /apitechu/v2/cuentas");

       var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";
       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient

       var identiban = req.body.IBAN; //  la variable ident es el identificador que llega en el parámetro

       var query = 'q={"IBAN":"' + identiban + '"}'; // la variable query es la condicion de que la id sea la que llega por parámetro

       console.log("query1" + query);
       console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){                                                        //con la url  para acceder a la coleccion user




          if (err){
            var response  = {"msg" : "Error obteniendo las cuentas por IBAN"}
            res.status(500);
          }  else {
                    if (body.length > 0 ){ // Si el body tiene contenido

                        Importeact = body[0].Importe + req.body.Importe;
                        console.log("importeact " + Importeact);
                        var putBody = '{"$set":{"Importe":' + Importeact + '}}';
                        console.log(" putbody " +  putBody);
                        httpClient.put("cuentas?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(errput, resMLabput, putBody){
                        console.log("usuario logado en Mlab");
                        var response = body;
                        console.log(body);
                        res.send(response); // Se envía el response
                      })
                      }
                    }

        });
        //  var response = !err ?    // condicion ternaria: Si no es error se responde el contenido del body y sino el mensaje de error
        //  body[0] : { "msg" : "Error obteniendo los usuarios"}
}


function createCuentaV2(req, res) {
         console.log("create post /apitechu/v2/cuentas");
         console.log("parámetros" );
         console.log( req.params);
         console.log("QueryString" );
         console.log( req.query);
         console.log("HEaders" );
         console.log(req.headers);
         console.log("Body" );
         console.log(req.body);



        var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";
        var httpClient = requestJson.createClient(baseMlabURL);

        if (req.body.Importe > 0){ // el importe incial siempre a de ser positivo

          var d = new Date();
          var time =d.getTime();
          var dia = d.getDate();
          var mes = (d.getMonth() + 1);
          var ano = (d.getFullYear());
          var fecha = dia + "-" + mes +"-" + ano;


          var iban = "ES" + req.body.IdUsuario + "000000"+ time;
          console.log("Fechas " + fecha);

           var newCuenta = { // Recuperamos estos parámetros del Body
             "iduser": req.body.IdUsuario,
             "IBAN": iban,
             "Concepto": req.body.Concepto,
             "Importe": req.body.Importe,
             "Fecha" : fecha
           }
           console.log( "datos de la cuenta " + newCuenta);
           httpClient.post("cuentas?" + mLabAPIKey, newCuenta, function(err, resMLab, body){


           console.log("cuenta creada en Mlab, vamos a dar de alta un movimiento");


               var newMovto ={
                      "iduser": req.body.IdUsuario,
                      "IBAN": iban,
                      "Concepto":  "Aportación de apertura",
                      "Importe": req.body.Importe,
                      "Fecha": fecha
                }
                console.log( "datos del movimiento " + newMovto);
                httpClient.post("movimientos?" + mLabAPIKey, newMovto, function(err, resMLab, body){
                      console.log("movimiento creado en Mlab");
                      res.status(201).send({"msg":"Alta Realizada"}); // entendemos que ha finalizado ok con code status 201
                }) // fin del alta de movimientos

          })
        }
}

function createCuentaByTokenV2(req, res) {
         console.log("create post /apitechu/v5/cuentas byToken  ................");
         console.log("parámetros" );
         console.log( req.params);
         console.log("QueryString" );
         console.log( req.query);
         console.log("HEaders" );
         console.log(req.headers);
         console.log("Body" );
         console.log(req.body);

         console.log("post  /apitechu/v5/cuentas/token");

         console.log("buscamos el id a través del token");

        var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/user";

         var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                                //con la url  para acceder a la coleccion user


         var token = req.body.token; //  la variable ident es el identificador que llega en el parámetro.

         // la variable query es la condicion de que la id sea la que llega por parámetro
          var query = 'q={"token": ' + token + '}';


         console.log(query);
         //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
         httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
           // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

            if (err){
              var response  = {"msg" : "Error obteniendo los usuarios"}
              res.status(500);
            }  else { if (body.length > 0 ){ // Si el body tiene contenido
                    console.log("encontrado el id de user" + body[0].iduser);
                    console.log("ahora le vamos a dar de alta una nueva cuenta");
                    console.log("cambiamos a la base de datas de cuentas ");

                    var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";
                    var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient

                    var iduser = body[0].iduser; //  la variable ident es el identificador que llega en el parámetro.
                    req.body.IdUsuario = body[0].iduser;
                    var query = 'q={"iduser":' + iduser + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

                    var d = new Date();
                    var time =d.getTime();
                    var dia = d.getDate();
                    var mes = (d.getMonth() + 1);
                    var ano = (d.getFullYear());
                    var fecha = dia + "-" + mes +"-" + ano;
                    var iban = "ES" + req.body.IdUsuario + "000000"+ time;
                    console.log("Fechas " + fecha);

                   var newCuenta = { // Recuperamos estos parámetros del Body
                             "iduser": req.body.IdUsuario,
                             "IBAN": iban,
                             "Concepto": req.body.Concepto,
                             "Importe": req.body.Importe,
                             "Fecha" : fecha
                   }
                   console.log( "datos de la cuenta " + newCuenta);
                   console.log(" vamos a dar la cuenta de alta");
                   httpClient.post("cuentas?" + mLabAPIKey, newCuenta, function(err, resMLab, body){


                   console.log("cuenta creada en Mlab, vamos a dar de alta un movimiento");
                   var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/movimientos";
                   var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                                          //con la url  para acceder a la coleccion user


                     var newMovto ={
                                      "iduser": req.body.IdUsuario,
                                      "IBAN": iban,
                                      "Concepto":  "Aportación de apertura",
                                      "Importe": req.body.Importe,
                                      "Fecha": fecha
                      }
                      console.log( "datos del movimiento " + newMovto);
                      httpClient.post("movimientos?" + mLabAPIKey, newMovto, function(err, resMLab, body){
                            console.log("movimiento creado en Mlab");
                            res.status(201).send({"msg":"Alta Realizada"}); // entendemos que ha finalizado ok con code status 201
                      }) // fin del alta de movimientos

                })
        }else{
          console.log("no ha recuperado datos")
          var response  = {"msg" : "Error obteniendo los usuarios"}
          res.status(401);
        }
      }
  })
}




// Se incluyen los module.exports de las funciones para que se puedan invocar desde fuera


module.exports.getCuentasByIdV2 = getCuentasByIdV2;
module.exports.getCuentasByTokenV2 = getCuentasByTokenV2;
module.exports.getCuentasByIBANV2 = getCuentasByIBANV2;
module.exports.postCuentasByIBANV2 = postCuentasByIBANV2;
module.exports.createCuentaV2 = createCuentaV2;
module.exports.createCuentaByTokenV2 = createCuentaByTokenV2;
