// Con el REQUIRE importamos el fichero desde la ruta indicada
const io = require('../io');

const crypt = require('../crypt');

const requestJson = require('request-json');



const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/user";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;// la clave ya no está en el código sino que es una referencia del fichero .env

console.log(mLabAPIKey);


function loginAuthV1(req, res) {

//  En el body he metido en formato Json los datos de un usuario
// {"email":"craymond3@blogs.com", "password":"zpntpz"}
       console.log("POST /apitechu/v1/loginAuth");

// Importamos el fichero usuarios.json sobre la variable users que es un array
       var users = require('../usuarios.json');


      console.log(req.body.email);
      console.log(req.body.password);



      var encontrado = false;
// Se define un objeto respuesta
      var respuesta = {};

      for (var i = 0; i < users.length; i++) {
        console.log(users[i].email);
        console.log(users[i].password);
           if ((users[i].email == req.body.email) && (users[i].password == req.body.password)){
             users[i].logged = true;
             encontrado = true;
             break
           }
      }

      if(encontrado){
        respuesta.mensaje ={ "mensaje" : "Login correcto", "idUsuario" : users[i].id  }
// para invocar al metodp write se escribe el nombre del fichero punto método
        io.writeUserDatatoFile(users);
      }else {respuesta.mensaje ={ "mensaje" : "Login incorrecto"}}

      console.log("respuesta: " + respuesta);
      res.send(respuesta);


}

function loginAuthV2(req, res) {

//  En el body he metido en formato Json los datos de un usuario
// {"email":"craymond3@blogs.com", "password":"zpntpz"}
       console.log("POST /apitechu/v2/loginAuth");

// Importamos el fichero usuarios.json sobre la variable users que es un array
       var users = require('../usuarios.json');


      console.log(req.body.email);
      console.log(req.body.password);

      console.log( " ***************      1            *********** ");



      var encontrado = false;
// Se define un objeto respuesta
      var respuesta = {};

      var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                             //con la url  para acceder a la coleccion user


      var emailparams = req.body.email; //  la variable email ent es el email que llega en el parámetro.

      console.log( " ***************      2            *********** ");


      var query = 'q={"email": "' + emailparams + '"}'; // la variable query es la condicion de que la id sea la que llega por parámetro

      console.log(query);
      //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
      httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
        // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

        console.log( " ***************      3            *********** ");
        console.log(body[0]);
        if (body.length > 0 ){
            var emailMlab = body[0].email;
            console.log("email " + emailMlab);
            var passwordlMlab = body[0].password;
            console.log("pass " + passwordlMlab);
            var idUser = body[0].iduser;
            console.log("id" + idUser);
            var nombre = body[0].first_name;
            var apellido = body[0].last_name;
          }


        var d = new Date();
        var time =d.getTime();
        console.log ("hora del systema " + time);

         if (err){
           var response  = {"mensaje" : "Error obteniendo los usuarios"}
           res.status(500);
           var response ={"mensaje" : "Usuario no encontrado"}
           res.send(response); // Se envía la respuesta]
         }  else { if (body.length > 0 ){ // Si el body tiene contenido
                      console.log( " ***************      4            *********** ");
                      if(  crypt.checkPassword(req.body.password, passwordlMlab)){
                          console.log( " ***************      5            *********** ");
                          //var respuesta = body[0];
                          console.log( " ¿Logado? " + body[0].logged);
                          if( body[0].logged){ // si está logado....
                             console.log("Usuario ya está logado");
                              var response ={ "mensaje" : "El Usuario ya está logado"}


                              res.send(response); // Se envía la respuesta]
                          }else{
                              console.log( " ***************      6            *********** ");
                              console.log(" vamos a logar el usuario " +  body[0].iduser);
                              var query = 'q={"iduser": ' + body[0].iduser +'}'; // la variable query es la condicion de que la id sea la que llega por parámetro
                              var d = new Date();
                              var time =d.getTime();

                              var tokenalfa = body[0].iduser + time.toString();

                              token =  parseInt(tokenalfa)

                              console.log("query " +  query);
                              console.log ("token = " + token );
                              if (body[0].iduser > 1 ){
                                var putBody = '{"$set":{"logged":true , "token": ' + token + '}}';


                              } else {
                                console.log(" no queremos meterle el logged");
                                var putBody = '{"$set":{"token": ' + token + '}}';

                              }


                              console.log(" putbody " +  putBody);

                              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(errput, resMLabput, putBody){

                              console.log("usuario logado en Mlab");

                              //var tokencrypt =   crypt.hash(token)// (constante punto metodo)
                              //console.log("token crypt" + tokencrypt);
                              var response ={ "mensaje" : "Login corrrecto", "idUsuario" : idUser, "token" : token,
                                                  "first_name" : nombre, "last_name" :apellido }


                              console.log("idUser" +  idUser)
                              console.log("token" +  token)
                              console.log("respuesta" +  response[0])

                             res.send(response); // Se envía la respuesta]

                              })
                          }

                      }else {
                        console.log("pasword incorrecta")
                        var response ={"mensaje" : "Password incorrecta"}
                        console.log("pass " + req.body.password +"-" + passwordlMlab);
                        res.send(response); // Se envía la respuesta]

                      }

                   } else {
                       console.log("usuario no encontrado")
                       var response ={ "mensaje" : "Usuario no encontrado"}
                       res.send(response); // Se envía la respuesta]
                         }
                 }
         console.log("response " + response);
         //res.send(respuesta); // Se envía la respuesta]
       });

}



function logoutAuthV2(req, res) {

         console.log(" logout ***************************")
         console.log("req.params.id,"  + req.body.token);

         console.log("token " + req.params.id + "-" + req.body.token)
         console.log("  LOGOUT - POST /apitechu/v2/logoutAuth " + req.body.token);
          console.log("req.params.id," +  req.body.token);
         var token = req.body.token; //  la variable ident es el identificador que llega en el parámetro.

         var query = 'q={"token": ' + token + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

         console.log("query " + query);

        //  var ident = req.params.id; //  la variable email ent es el identificador que llega en el parámetro.
         //
        //  var query = 'q={"iduser":' + ident + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro
         //
        //  console.log(query);
         var response = {};

         var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                                //con la url  para acceder a la coleccion user
         console.log(query);
         //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
         httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
           // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

            if (err){
              var response  = {"mensaje" : "Error obteniendo los usuarios"}
              res.status(500);
              var response ={"mensaje" : "Usuario no encontrado"}
              res.send(response); // Se envía la respuesta]

            }  else { if (body.length > 0 ){ // Si el body tiene contenido

                             if( body[0].logged){ // si está logado....
                                  console.log(body);
                                  var emailMlab = body[0].email;
                                  console.log("email " + emailMlab);
                                  var passwordlMlab = body[0].password;
                                  console.log("pass " + passwordlMlab);
                                  var idCliente = body[0].id;
                                  console.log("id" + idCliente);


                                 //var respuesta = body[0];
                                 //var putBody = '{"$set":{"logged":true , "token": [ ' + token + ']}}';
                                  var putBody = '{"$unset":{"logged":"", "token" : ""}}';

                                   httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(errput, resMLabput, bodyput){
                                     console.log("usuario ha cerrado sesion en Mlab");
                                     //res.status(201).send({"msg":"usuario guardado"}); // entendemos que ha finalizado ok con code status 201
                                     //var respuesta ={ "mensaje" : "Logout correcto", "idUsuario" : idCliente }
                                     response = { "mensaje" : "Logout correcto" , "idUsuario" : idCliente}
                                     //var respuesta = { "Logout correcto"  : idCliente }
                                     res.send(response); // Se envía la respuesta]
                                     res.status(201);
                                   })

                             }else {
                               var response ={"mensaje" : "El usuario No estaba logado", "idUsuario" : idCliente }
                               res.send(response); // Se envía la respuesta]
                               res.status(404);
                             }

                      } else {
                          var response ={"mensaje" : "Claves Incorrectas", "idUsuario" : idCliente }
                          res.send(response); // Se envía la respuesta]
                          res.status(404);
                            }
                    }
            console.log("respuesta" + response);
            //res.send(respuesta); // Se envía la respuesta]
          });
}


function logoutAuthV1(req, res) {
         console.log("POST /apitechu/v1/logoutAuth");
         console.log(req.body.id);
         var users = require('../usuarios.json');

         var encontrado = false;
         var logado  = false;
         var respuesta = {};

         for (var i = 0; i < users.length; i++) {
           console.log(users[i].id);

              if (users[i].id == req.body.id) {
                  console.log(" encontrado");
                    encontrado = true;
                  if(users[i].logged){
                      console.log(" logado")
                      delete users[i].logged
                      logado = true;
                  }
                break
              }
         }

         if(logado){
           respuesta ={ "mensaje" : "Logout correcto", "idUsuario" : users[i].id  }

           io.writeUserDatatoFile(users);
         }else {respuesta ={ "mensaje" : "Logout incorrecto"}}

         console.log("respuesta: " + respuesta);
         res.send(respuesta);


}


function passwordtAuthV1(req, res) { //modificar pass. Necesita Id y Password
         console.log("POST /apitechu/v1/logoutAuth");
         console.log(req.body.id, req.body.password);
         var users = require('../usuarios.json');

         var encontrado = false;
         var logado  = false;
         var respuesta = {};

         for (var i = 0; i < users.length; i++) {
           console.log(users[i].id);

              if (users[i].id == req.body.id) {
                  console.log(" encontrado");
                    encontrado = true;
                  if(users[i].logged){
                      console.log(" logado")
                      users[i].password=req.body.password;
                      logado = true;
                  }
                break
              }
         }

         if(logado){
           respuesta.mensaje ={ "mensaje" : "password Modificada", "idUsuario" : users[i].id  }
           io.writeUserDatatoFile(users);
         }else {respuesta.mensaje ={ "mensaje" : "No se ha modificado la Password"}}

         console.log("respuesta: " + respuesta);
         res.send(respuesta);
}

function passwordtAuthV2(req, res) { //modificar pass. Necesita Id y Password
         console.log("POST /apitechu/v2/password");
         console.log("req.params.id," + req.body.password + "-" + req.body.token);

         var users = require('../usuarios.json');

         var encontrado = false;
         var logado  = false;
         var response = {};

         var token = req.body.token; //  la variable ident es el identificador que llega en el parámetro.

         var query = 'q={"token": ' + token + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

         console.log(query);

        //  var ident = req.params.id; //  la variable email ent es el identificador que llega en el parámetro.
         //
        //  var query = 'q={"id":' + ident + '}' ; // la variable query es la condicion de que la id sea la que llega por parámetro


         var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                                //con la url  para acceder a la coleccion user
         console.log("qyery " + query);
         //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
         httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
           // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

            if (err){
              var response  = {"mensaje" : "Error obteniendo los usuarios"}
              res.status(500);
              var response ={"mensaje" : "Usuario no encontrado"}
              res.send(response); // Se envía la respuesta]

            }  else { if (body.length > 0 ){ // Si el body tiene contenido



                            //  if((body[0].logged)&&(crypt.checkPassword(body[0].token, req.params.token))){ // si está logado y coincide el Token
                                  console.log(body);
                                  var body_lab = body[0];
                                  console.log(body_lab);
                                  console.log("password antigua:    ---");
                                  console.log(body_lab.password);
                                  console.log("password Nueva");
                                  console.log(req.body.password);
                                  var emailMlab = body[0].email;
                                  console.log("email " + emailMlab);
                                  var passwordlMlab = body[0].password;

                                  console.log("pass " + passwordlMlab);
                                  var passnew = crypt.hash(req.body.password);

                                  var idCliente = body[0].id;
                                  console.log("id" + idCliente);


                                 //var respuesta = body[0];
                                   var putBody = '{"$set":{"password":"'+ passnew + '"}}';

                                   httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(errput, resMLabput, bodyput){
                                   console.log("Password Modificada");

                                   response = { "mensaje" : "Password Modificada" , "idUsuario" : idCliente, "token" : token }
                                     res.status(201);
                                     res.send(response); // Se envía la respuesta]

                                    })

                            //  }else {
                            //    var respuesta ={"msg" : "El usuario No estaba logado", "idUsuario" : idCliente, "token" : token }
                            //    res.send(respuesta); // Se envía la respuesta]
                            //    res.status(404);
                             //}

                      } else {
                          console.log(" no ha encontrado token " + token );
                          var response ={"mensaje" : "Claves Incorrectas", "token" : token }
                          res.status(404);
                          res.send(response); // Se envía la respuesta]
                            }
                    }
            console.log("response" + response);
          });
}




module.exports.loginAuthV1 = loginAuthV1;
module.exports.loginAuthV2 = loginAuthV2;

module.exports.logoutAuthV1 = logoutAuthV1;
module.exports.logoutAuthV2 = logoutAuthV2;

module.exports.passwordtAuthV1 = passwordtAuthV1;
module.exports.passwordtAuthV2 = passwordtAuthV2;
