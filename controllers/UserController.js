// Con el REQUIRE importamos el fichero desde la ruta indicada
const io = require('../io');

const crypt = require('../crypt');

const requestJson = require('request-json');



var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/user";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;// la clave ya no está en el código sino que es una referencia del fichero .env

console.log(mLabAPIKey);




function getUsersV2(req, res) { // Nos va a devolver la coleccion de usuarios
       console.log("GET /apitechu/v2/users");

       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                               //con la url  para acceder a la coleccion user


      // baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/user";


       console.log("getUsersV2");

       //invocamos al get  con (la base de la MlabURL + ?  de la condición + la APIKEY) y una funcion con tres parámetros
       //  Como condición es la s de ordenacion y la f para que no saque el campo _id
       // httpClient.get("user?s={'id':1}&f={'_id':0}&" + mLabAPIKey, function(err, resMLab, body){
       httpClient.get("user?s={'iduser':1})&" + mLabAPIKey, function(err, resMLab, body){
       //httpClient.get("user?s={'iduser':-1}&l=1&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          var response = !err ?    // condicion ternaria:
          //Si no es error se rellena el response con el contenido del body y sino con el mensaje de error
            body : { "msg" : "Error obteniendo los usuarios"}



            res.send(response); // Se envía el response
       })


}

function getUsersByTokenV2(req, res) { // devuelve un usuario con un token concreto
       console.log("GET /apitechu/v3/users/:token --   busqueda de usuario por token " + req.params.id) ;

       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                              //con la url  para acceder a la coleccion user


       var token = req.params.id; //  la variable ident es el identificador que llega en el parámetro.

       var query = 'q={"token": ' + token + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

       console.log(query);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          if (err){
            var response  = {"msg" : "Error obteniendo los usuarios"}
            res.status(500);
            res.send(response); // Se envía el response
          }  else { if (body.length > 0 ){ // Si el body tiene contenido
                        console.log("ha encontrado al usuario" +  body[0])
                        var response = body[0];
                        res.status(201);
                        res.send(response); // Se envía el response
                    } else {
                        var response ={"msg" : "Usuario no encontrado"}
                        res.status(404);
                        res.send(response); // Se envía el response
                          }

                  }


        });
        //  var response = !err ?    // condicion ternaria: Si no es error se responde el contenido del body y sino el mensaje de error
        //  body[0] : { "msg" : "Error obteniendo los usuarios"}
}


function getUsersByIdV2(req, res) { // devuelve un usuario con una id concreta
       console.log("GET /apitechu/v2/users/:id");

       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                              //con la url  para acceder a la coleccion user


       var ident = req.params.id; //  la variable ident es el identificador que llega en el parámetro.

       var query = 'q={"iduser":' + ident + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

       console.log(query);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          if (err){
            var response  = {"msg" : "Error obteniendo los usuarios"}
            res.status(500);
          }  else { if (body.length > 0 ){ // Si el body tiene contenido
                        var response = body[0];

                    } else {
                        var response ={"msg" : "Usuario no encontrado"}
                        res.status(404);
                          }

                  }

          res.send(response); // Se envía el response
        });
        //  var response = !err ?    // condicion ternaria: Si no es error se responde el contenido del body y sino el mensaje de error
        //  body[0] : { "msg" : "Error obteniendo los usuarios"}
}




function createUserV2(req, res) {
         console.log("POST /apitechu/v2/users");

        console.log("first_name" + req.body.first_name);

        console.log("last_name" + req.body.last_name);
         var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                                //con la url  para acceder a la coleccion user
         var email = req.body.email; //  la variable email ent es el email que llega en el parámetro.
         var query = 'q={"email": "' + email + '"}'; // la variable query es la condicion de que la id sea la que llega por parámetro
         console.log(query);
         //invocamos al get  con (la http de la  user + ? condicion query + la APIKEY) y una funcion con tres parámetros
         httpClient.get("user?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
           var emailValidada =  crypt.checkEmail(email);
           if (email == "email"){
             emailValidada = "OK";
           }
           if (emailValidada == "OK"){ //  si el email es correcto
               console.log ("   El Email es correcto  ");
               if ((!err) && (body.length < 1)){ // no existe ese email en el sistema
                 console.log ( " El Email es nuevo en el sistema. NO HAY USUARIOS CON ESTE MISMO EMAIL ");
                 httpClient.get("user?s={'iduser':-1}&l=1&" + mLabAPIKey, function(err, resMLab, body){

                  //  vamos a ver cual es el id para darle de alta
                  console.log("le vamos a asignar un id. El último asignado era el  " + body[0].iduser);
                  if ((!err) && (body[0].iduser > 0 )){ // si ha obtenido una iduser valido

                      var totusu  = body[0].iduser;
                      totusu = totusu +1 ;

                      var iduser = totusu;
                      console.log ("El nuevo id de usuario es el " + iduser );


                      var nombre = req.body.first_name;
                      var apellido = req.body.last_name;
                      var correo =  req.body.email;
                      var palabraclave = req.body.password;

                      var d = new Date();
                      var time =d.getTime();

                      var tokenalfa = iduser + time.toString();

                      token =  parseInt(tokenalfa)



                      var newUser = { // Recuperamos estos parámetros del Body
                                    "iduser": iduser,
                                    "first_name": nombre,
                                    "last_name": apellido,
                                    "email": correo,

                                    "logged":true,
                                    "token": token,

                                    // vamos a encriptar la password
                                    "password" : crypt.hash(palabraclave),// (constante punto metodo)
                                    };
                      console.log (" datos del usuario " + newUser);
                      console.log ("le vamos a dar de alta");

                      // var tokencrypt =   crypt.hash(token)// (constante punto metodo)
                      // console.log("token crypt" + tokencrypt);

                      httpClient.post("user?" + mLabAPIKey, newUser, function(err, resMLab, body){
                                      console.log("Usuario dado del alta en Mlab");

                     if (!err){

                                console.log("Vamos a dar de alta la cuenta ");
                                // Para el post se necesitan tres parámetros: la http  , los datos del cliente y la funcion
                                // Damos de alta una cuenta asociada

                                var iban = "ES" + iduser + "000000"+ time;

                                var d = new Date();
                                var time =d.getTime();
                                var dia = d.getDate();
                                var mes = (d.getMonth() + 1);
                                var ano = (d.getFullYear());
                                var fecha = dia + "-" + mes +"-" + ano;

                                var iban = "ES" + iduser + "000000"+ time;


                                console.log("Fechas " + fecha);

                                var newCuenta = { // Recuperamos estos parámetros del Body
                                  "iduser": iduser,
                                  "IBAN": iban,
                                  "Concepto":  "Cuenta Nomina",
                                  "Importe": 500,
                                  "Fecha" : fecha
                                }
                                console.log( "datos de la cuenta " + newCuenta);
                                httpClient.post("cuentas?" + mLabAPIKey, newCuenta, function(err, resMLab, body){
                                    console.log("cuenta creada en Mlab, vamos a dar de alta un movimiento");
                                   // Para el post se necesitan tres parámetros: la http  , los datos del cliente y la funcion
                                   var newMovto ={
                                          "iduser": iduser,
                                          "IBAN": iban,
                                          "Concepto":  "Aportación de apertura",
                                          "Importe": 500,
                                          "Fecha": fecha
                                    }
                                    httpClient.post("movimientos?" + mLabAPIKey, newMovto, function(err, resMLab, body){
                                        console.log("movimiento creado en Mlab");
                                        res.status(201);
                                        var response  = {"mensaje":"Alta Realizada", "idUsuario": iduser, "token": token}
                                        res.send(response);

                                    }) // fin del alta de movimientos

                                }) // fin del alta de cuentas
                              }// SI NO HAY ERRORES....
                          })  // fin del alta de usuarios
                       } //  la id es valida
                     }) //  recuperar la última id
                  } else {
                    console.log("usuario ya existe en el sistema");
                    
                    var response  = {"mensaje" :"Ya existe este mismo email en el sistema"}
                    res.send(response);


                  } // comprobar si existe un emil igual en el sistema


      } else {
        console.log("email no es válida");

        var response  = {"mensaje" :"email no es válida"}
        res.send(response);

      }  //  email correcto
    })  // fin del acceso a usuarios por email
}


/*


                          var putBody = '{"$set":{"totctas":"'+ totusu + '"}}';
                          req.body.iduser = totusu;
                          console.log("req.body.iduser" + req.body.iduser);



                          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(errput, resMLabput, bodyput){
                                console.log("total de usuarios Modificada");

                                console.log("    Vamos a dar de alta el nuevo usuario   ");
                                console.log ("req.body.iduser " + req.body.iduser);

                                var idusuario = req.body.iduser;



}



*/


function getUsersV1(req, res) {
       console.log("GET /apitechu/v1/users");


       // res.sendfile('usuarios.json', {root: __dirname}); // envía respuesta del fichero de usuarios.json y la ruta

//   Tambien podemos llevarlo a un array y responder con el array

       // var users = require('../usuarios.json'); // Lo guardamos en el array
       // res.send(users); // envía como respuesta el array
       //

       var respuesta = {};
       var users = require('../usuarios.json'); // guardamos el fichero en el array
//                         filtros
//  los filtros se identifican con un $       $count $top
       if (req.query.$count == "true") {
         console.log("Count needed");
         respuesta.count = users.length;
       }
// condicion ternaria. La condicion está despues del igual
       respuesta.users = req.query.$top ?
// Si es true -  se ejecuta la instrucción antes de los :
// Si es false - se ejecuta la instrccion despues de los :

// utilizamos el slice para borrar un registro del Array

         users.slice(0, req.query.$top) : users;


       res.send(respuesta);


}



function deleteUserV1(req, res) {
         console.log("DELETE /apitechu/v1/users/:id");
         console.log("id es " + req.params.id);

         var users = require('../usuarios.json');
         var deleted = false;

         //  1. for
         //  console.log("Usando for normal 1");
         //  for (var i = 0; i < users.length; i++) {
         //     console.log("comparando " + users[i].id + " y " +  req.params.id);
         //     if (users[i].id == req.params.id) {
         //        console.log("La posicion " + i + " coincide");
         //        users.splice(i, 1);
         //        deleted = true;
         //        break;
         //     }
         //  }

         //  2. for each. Es un método.
         //   NO permite un BREAK. RECORRE TODA LA TABLA
         //
         //   Los Arrays tienen una funcion forEach, que tiene un parámetro
         //   que es una funcion que tiene tres  parámeros (elemento, indice array)
         //
         //     console.log("Usando Array ForEach");
         //     users.forEach(function (user, index) {
         //        console.log("comparando " + user.id + " y " +  req.params.id);
         //        if (user.id == req.params.id) {
         //            console.log("La posicion " + index + " coincide");
         //            users.splice(index, 1);
         //            deleted = true;
         //        }
         //     });
         //

         //  3. for in   for element in object
         // for elemento (of / in) TABLA  Ejemplo usuario (of/in) usuarios
         // IN - trabaja en las propiedades enumerables de un objeto. Saca el indice, no el valor del id
         //      for (var user in users) {
         //         console.log(`users.${user} = ${users[user].id}`);
         //         if ((users[user].id) == req.params.id
         //             deleted = true;
         //             users.splice(user , 1);
         //             break;
         //
         //         }
         //       }
         //
         //      FOR IN de Carlos
         //
         //      console.log("Usando for in");
         //      for (arrayId in users) {
         //         console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
         //         if (users[arrayId].id == req.params.id) {
         //           console.log("La posicion " + arrayId " coincide");
         //           users.splice(arrayId, 1);
         //           deleted = true;
         //           break;
         //         }
         //      }
         //
         //
         //  4.  for   of
         // OF - for(elemento of tabla)  va por cada uno de los valores de los elementos
         // Se puede saber el valor pero no el índice.
         // soluciones:
         // 1. Destructuración con un entries, que me devuelve el indice y el valor del array
         // que se guardan en las dos variables que hemos creado i, valor
         //
         // 2. Otra solución pasaría por creame un contador que haga la función de indice
         //
         //    console.log("Usando for of 1");
         //    for (user of users) {
         //        console.log("comparando " + user.id + " y " +  req.params.id);
         //        if (user.id == req.params.id) {
         //            console.log("La posición ? coincide");
         //    // Which one to delete? order is not guaranteed...
         //            deleted = false;
         //            break;
         //        }
         //    }
         //
         //    console.log("Usando for of 2");
         //    Destructuring, nodeJS v6+
         //    for (var [index, user] of users.entries()) {
         //        console.log("comparando " + user.id + " y " +  req.params.id);
         //        if (user.id == req.params.id) {
         //           console.log("La posicion " + index + " coincide");
         //           users.splice(index, 1);
         //           deleted = true;
         //           break;
         //        }
         //    }
         //
         //    console.log("Usando for of 3");
         //    var index = 0;
         //    for (user of users) {
         //        console.log("comparando " + user.id + " y " +  req.params.id);
         //        if (user.id == req.params.id) {
         //           console.log("La posición " + index + " coincide");
         //           users.splice(index, 1);
         //           deleted = true;
         //           break;
         //        }
         //        index++;
         //    }

         //  5.  for   findIndex   LANDAS
         //
         // console.log("Usando Array findIndex");
         // var indexOfElement = users.findIndex(     // Busco el índice del elemento
         //   function(element){
         //     console.log("comparando " + element.id + " y " +   req.params.id);
         //     return element.id == req.params.id
         //   }
         // )
         //
         // console.log("indexOfElement es " + indexOfElement);
         // if (indexOfElement > 0) { // si es mayor de cero es que lo ha encontrado
         //   users.splice(indexOfElement, 1);
         //   deleted = true;
         // }





         console.log("Usando for of 2");
         // Destructuring, nodeJS v6+
         for (var [index, user] of users.entries()) {
           console.log("comparando " + user.id + " y " +  req.params.id);
           if (user.id == req.params.id) {
             console.log("La posicion " + index + " coincide");
             users.splice(index, 1);
             deleted = true;
             break;
           }
         }

         if (deleted) {
           io.writeUserDatatoFile(users);
         }

//  Condición ternaria. la condición está despues del  =
//  si es true se rellena con la sentencia 1 "Usuario borrado"
//  si es false se rellena con la sentencia 2 "Usuario no encontrado"
         var msg = deleted ?
           "Usuario borrado" : "Usuario no encontrado."

         console.log(msg);
         res.send({"msg" : msg});
}


function deleteUserV2(req, res) {
         console.log("DELETE /apitechu/v2/users/:_id");
         console.log("id es " + req.params.id);


         var ident = req.params.id; //  la variable email ent es el identificador que llega en el parámetro.

         var queryget = 'q={"id":' + ident + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

         console.log(queryget);
         var respuesta = {};

         var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                                //con la url  para acceder a la coleccion user

         //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
         httpClient.get("user?" + queryget + "&" + mLabAPIKey, function(err, resMLab, body){
           // resMLab ES LA RESPUESTA DESDE EL SERVIDOR


          if (err){
              res.status(500).send({"msg":"Error al borrar el usuario"});

          }  else {
              console.log("delete");
              console.log(body);
              var emailMlab = body[0].email;
              console.log("email " + emailMlab);
              var passwordlMlab = body[0].password;
              console.log("pass " + passwordlMlab);
              var idLab = body[0]._id;
              console.log("    --------      idLab      ------------ " );
              console.log( idLab);
              var idCliente =  body[0].id;

              var querydel = 'q={"_id": '  + body[0]._id;  // la variable query es la condicion de que la id sea la que llega por parámetro

              //  "_id": { "$oid": "5c51b9dd1f6e4f4ca3e238c0" }         { '$oid': '5c51b9dd1f6e4f4ca3e238c0' }
              // idlab  ==>   { '$oid': '5c51b9dd1f6e4f4ca3e238c0' }
              //  querydel   ==>   q={"_id": [object Object]

              console.log(querydel);

              httpClient.DELETE("user?" + queryget + "&" + mLabAPIKey, function(err, resMLab, body){
                    if (err){
                         console.log("Error al borrar");
                         res.status(500);
                         var respuesta ={"msg" : "Error al borrar el cliente"}
                         res.send(respuesta); // Se envía la respuesta]

                     }else {
                         res.status(201).send({"msg":"usuario borrado", "id" : idCliente}); // entendemos que ha finalizado ok con code status 201
                     }
              })
          }


        })


}



function createUserV1(req, res) {
         console.log("POST /apitechu/v1/users");
         console.log("parámetros" );
         console.log( req.params);
         console.log("QueryString" );
         console.log( req.query);
         console.log("HEaders" );
         console.log(req.headers);
         console.log("Body" );
         console.log(req.body);

         console.log(req.body.first_name);
         console.log(req.body.last_name);
         console.log(req.body.email);

         var newUser = {
           "first_name": req.body.first_name,
           "last_name": req.body.last_name,
           "email": req.body.email
         }

         var users = require('../usuarios.json');
//  PUSH es un método para insertar al final del Array. Se incluye en memoria
         users.push(newUser);
//  Se escribe el array en el ficero. con el nombre del fichero punto metodo
         io.writeUserDatatoFile(users);
         console.log("Usuario añadido con éxito");

         res.send({"msg" : "Usuario añadido con éxito"})
}





// Se incluyen los module.exports de las funciones para que se puedan invocar desde fuera

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.getUsersByTokenV2 = getUsersByTokenV2;


module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;



module.exports.deleteUserV1 = deleteUserV1;
module.exports.deleteUserV2 = deleteUserV2;
