require('dotenv').config();// para que funcione hay que meter la informacion en el fichero .env

// Cargar en la constante express. require de la librería express que es un fichero que está en node modules
const express = require('express');
//
// Definimos la constante app que es igual a invocar a la funcion express(), que estará definida en
// la librería express.
// Inicializa el framework express en una constante.
// Se usará para ir llamando a todas las funciones del framework express
const app = express();
//

// Define la constante puerto  con un valor,
// Es el puerto en el que escuchar (será la variable de entorno PORT o si no
// esta definida,  será el valor 3000)
const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}


// preprocesado del Body. Asume que el BOdy es JSON
app.use(express.json());

app.use(enableCORS);


// RUTA DEL CONTROLADOR DE USUARIOS. Se establece un alias para invocar a la función
const userController = require('./controllers/UserController');

// RUTA DEL CONTROLADOR DE Autorizaciones
const authController = require('./controllers/AuthController');

// RUTA DEL CONTROLADOR DE Cuentas
const cuentasController = require('./controllers/CuentasController');

// RUTA DEL CONTROLADOR DE Movimientos
const movimientosController = require('./controllers/MovimientosController');

// RUTA DEL CONTROLADOR DE Integridad de Datos del Fichero
const dataController = require('./controllers/DataController');

// Iniciar un servidor
app.listen(port);

console.log("API escuchando en el puerto BIP BIP BIP " + port);

// La librería expess tiene una serie de metodos que se corresponden con
// las funcionalidades básicas Create (post) Read (get) Update (put/patch) deleted(delete)
// que tienen asociadas unos métodos manejadores
// Para invocar a estos métodos, son necesarios dos parámetros:
// 1- ruta, que es la que llega en el Postman.
// 2 -Funcion, que a su vez tiene dos parámetros que son son:
//        1 - Req - Requerimiento o petición
//        2 - Res - Respuesta
//        --------------------------
//         Modelo Vista Controlador
//        --------------------------
//  Para organizar el código:
//  Las funciones se desarrollan en los controladores. Para invocarlas,
//  se hace con la  nomenclatura del punto nombre del controlador + "." +  función

// REgistramos la ruta
// Invocamos a las funciones que están desarrolladas en UserController.js

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v3/users/:id', userController.getUsersByTokenV2);
app.get('/apitechu/v2/users/:id', userController.getUsersByIdV2);
;


app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);


app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);
app.delete('/apitechu/v2/users/:id', userController.deleteUserV2);


// Invocamos a las funciones que están desarrolladas en AuthController.js

app.post('/apitechu/v1/loginAuth', authController.loginAuthV1);
app.post('/apitechu/v2/loginAuth', authController.loginAuthV2);

app.post('/apitechu/v1/logout/:id', authController.logoutAuthV1);
app.post('/apitechu/v2/logout/:id', authController.logoutAuthV2);

app.patch('/apitechu/v1/password/:id', authController.passwordtAuthV1);
app.post('/apitechu/v2/password/:id', authController.passwordtAuthV2);


// Invocamos a las funciones que están desarrolladas en DataController.js

app.post('/apitechu/v1/datos', dataController.cleanDataV1);

// Invocamos a las funciones que están desarrolladas en CuentasController.jsgetCuentasByIBANV2

app.get('/apitechu/v2/cuentas/:id', cuentasController.getCuentasByIdV2);
app.get('/apitechu/v3/cuentas/:token', cuentasController.getCuentasByTokenV2);
app.get('/apitechu/v2/cuenta/:IBAN', cuentasController.getCuentasByIBANV2);
app.post('/apitechu/v3/cuenta', cuentasController.postCuentasByIBANV2);
app.post('/apitechu/v4/cuenta', cuentasController.createCuentaV2);
app.post('/apitechu/v5/cuenta', cuentasController.createCuentaByTokenV2);





// Invocamos a las funciones que están desarrolladas en MovimientosController.js

app.get('/apitechu/v2/movimientos/:IBAN', movimientosController.getMovimientosByIBANV2);
app.post('/apitechu/v2/movimientos', movimientosController.createMovimientoV2)
app.post('/apitechu/v2/traspasos', movimientosController.createTraspasoV2)
